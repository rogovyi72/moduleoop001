#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include "Birds.h"



int main(){
    std::vector<std::unique_ptr<Bird>> poultryFarm;
    poultryFarm.reserve(10);
    for (int i = 0; i<10; i++){
        if (rand()%2==0) {
            poultryFarm.push_back(std::make_unique<Sparrow>("blablabla"));
        }
        else{
            poultryFarm.push_back(std::make_unique<Nightingale>("kykareky"));
        }
    }

    std::for_each(poultryFarm.begin(), poultryFarm.end(), [](const auto& bird){
        bird->sing();
    });

};