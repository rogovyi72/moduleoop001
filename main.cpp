#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <cassert>

int main(){
    int number = 0;
    int sum = 0;
    std::vector<int> numbers;

    while (sum<50){
        std::cout<<"Enter the value:";
        std::cin>>number;
        sum+=number;
        numbers.push_back(number);
    }

    std::sort(numbers.begin(), numbers.end());

    assert(!numbers.empty());

    if (numbers.size()==1){
        std::cout<<"Min value: "<<numbers.front()<<std::endl;
    }
    else {
        std::cout<<"Min values: "<<*numbers.begin()<<" "<<*(++numbers.begin())<<std::endl;
    }

    return 0;
}