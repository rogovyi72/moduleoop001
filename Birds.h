#ifndef MODULE_BIRDS_H
#define MODULE_BIRDS_H

#include <string>


class Bird {
public:
    explicit Bird(const std::string& song);
    virtual void sing() const = 0;
protected:
    std::string m_song;
};

class Nightingale: public Bird{
public:
    explicit Nightingale(const std::string& song);
    void sing() const final;
};

class Sparrow: public Bird{
public:
    explicit Sparrow(const std::string& song);
    void sing() const final;
};



#endif //MODULE_BIRDS_H
