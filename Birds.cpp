#include "Birds.h"
#include <iostream>

Bird::Bird(const std::string &song):m_song(song) {

}

Nightingale::Nightingale(const std::string &song) : Bird(song) {

}

void Nightingale::sing() const {
    std::cout<<m_song<<std::endl;
}

Sparrow::Sparrow(const std::string &song) : Bird(song) {

}

void Sparrow::sing() const {
    std::cout<<m_song.substr(0,3)<<std::endl;
}
